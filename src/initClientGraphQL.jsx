import { ApolloClient, InMemoryCache, createHttpLink, ApolloLink } from '@apollo/client';
import { onError } from '@apollo/client/link/error';
import { setContext } from '@apollo/client/link/context';

const authLink = setContext((_, { headers }) =>
  // const token = getItem(TYPE_LOCAL_STORAGE.TOKEN);
  // const token = `eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNjUwMjUxNTIyLCJleHAiOjE2NTAzMzc5MjJ9.kh3bzr5VyfJ9tbQX8isaIkqXa4wsamZbcMu18Id24aM`;
  ({
    headers: {
      ...headers,
      'Access-Control-Allow-Origin': '*',
      // Authorization: token ? `Bearer ${token}` : '',
      'X-API-Key': '8e0935fe948ccf727342e86015c00834',
    },
  })
);

const link = onError(({ graphQLErrors, networkError }) => {
  if (graphQLErrors) {
    console.log(graphQLErrors);
  }
  if (networkError) {
    // if (networkError.message.includes('401')) {
    // }
    console.log(`[Network error]: ${networkError}`);
  }
});

const httpLink = createHttpLink({
  uri: 'http://pimtest.local/pimcore-graphql-webservices/events',
});

export const clientGraphQL = new ApolloClient({
  link: ApolloLink.from([link, authLink.concat(httpLink)]),
  cache: new InMemoryCache(),
  defaultOptions: {
    watchQuery: {
      fetchPolicy: 'no-cache',
      errorPolicy: 'ignore',
    },
    query: {
      fetchPolicy: 'no-cache',
      errorPolicy: 'all',
    },
    mutate: {
      fetchPolicy: 'no-cache',
      errorPolicy: 'all',
    },
  },
});
