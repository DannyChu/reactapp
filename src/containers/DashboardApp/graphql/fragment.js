import { gql } from '@apollo/client';

export const PLACE_TYPE_OUTPUT = gql`
  fragment PlaceTypeOutput on PlaceType {
    id
    language {
      languageName
    }
    languageCode
    typeName
    description
    places {
      placeName
    }
  }
`;

export const EVENT_LISTING_OUTPUT = gql`
  fragment EventListingOutput on EventEdge {
    id
    fromDate
    fromTime
    locationAddress
  }
`;
