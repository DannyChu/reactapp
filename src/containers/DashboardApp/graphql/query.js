import { gql } from '@apollo/client';
import { EVENT_LISTING_OUTPUT, PLACE_TYPE_OUTPUT } from './fragment';

export const GET_EVENT_LISTING = gql`
  query getEventListing(
    $ids: String
    $fullpaths: String
    $defaultLanguage: String
    $first: Int
    $after: Int
    $sortBy: [String]
    $sortOrder: [String]
    $filter: String
    $published: Boolean
  ) {
    getEventListing(
      id: $ids
      after: $first
      after: $after
      fullpaths: $fullpaths
      defaultLanguage: $defaultLanguage
      sortBy: $sortBy
      sortOrder: $sortOrder
      filter: $filter
      published: $published
    ) {
      edges {
        node {
          ...EventListingOutput
        }
      }
      totalCount
      # pageInfo {
      #   perPage
      #   itemCount
      #   currentPage
      #   pageCount
      # }
    }
  }
  ${EVENT_LISTING_OUTPUT}
`;

export const GET_PLACE_TYPE = gql`
  query placeTypePage($languageCode: String, $searchValue: String, $currentPage: Int!, $pageSize: Int!) {
    placeTypePage(
      input: { typeName: $searchValue, languageCode: $languageCode }
      page: { page: $currentPage, pageSize: $pageSize }
    ) {
      rows {
        ...PlaceTypeOutput
      }
      pageInfo {
        perPage
        itemCount
        currentPage
        pageCount
      }
    }
  }
  ${PLACE_TYPE_OUTPUT}
`;
