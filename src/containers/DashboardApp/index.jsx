import { useQuery } from '@apollo/client';
import React, { memo, useMemo, useEffect, useCallback, useState } from 'react';
import { GET_EVENT_LISTING } from './graphql/query';

export const DashboardApp = memo(() => {
  const [skip, setSkip] = useState(false);
  const [data, setData] = useState([]);

  const variablesQuery = useMemo(
    () => ({
      first: 10,
      after: 0,
    }),
    []
  );
  //! callback get query
  const onLoadSuccess = useCallback((value) => {
    console.log(value);
    // console.log(value.placeTypePage.rows);
    // setData(value.placeTypePage.rows);
    setSkip(true);
  }, []);

  // NOTE get list graphql
  const { loading, refetch } = useQuery(GET_EVENT_LISTING, {
    skip,
    onCompleted: onLoadSuccess,
    variables: variablesQuery,
    fetchPolicy: 'network-only',
  });

  const recallQuery = useCallback(() => {
    setSkip(false);
    refetch();
  }, [refetch]);

  useEffect(() => {
    recallQuery();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div>
      {loading && <span>Loading</span>}
      {/* {data.map((item) => (
        <div key={item.id}>{item.typeName}</div>
      ))} */}
    </div>
  );
});
